#ifndef TRACE_HH
#define TRACE_HH

#include <list>
#include <fstream>
#include <stddef.h>
#include <stdint.h>

#include "qq.hh"

struct Connection
{
	size_t bytes;
	uint64_t startTime;
	uint64_t duration;
	
	Connection(size_t bytes, uint64_t startTime, uint64_t duration)
		: bytes(bytes), startTime(startTime), duration(duration) {}
	
	Connection(const std::string &line);
};

class TraceReader
{
public:
	virtual ~TraceReader() = 0;
	
	virtual Connection *nextConnection() = 0;
};

class JITTraceReader: public TraceReader
{
	std::ifstream trace;	
	
public:
	JITTraceReader(const char *fileName)
		: trace(fileName, std::ios_base::in) {}
	
	virtual ~JITTraceReader();
	
	virtual Connection *nextConnection();
};

class PreloadingTraceReader: public TraceReader
{
	std::list<Connection *> connections;
	
public:
	PreloadingTraceReader(const char *fileName);
	
	virtual ~PreloadingTraceReader();
	
	virtual Connection *nextConnection();
};

#endif // TRACE_HH
