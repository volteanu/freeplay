#include <iostream>
#include <exception>
#include <stdlib.h>
#include <list>
#include <fstream>
#include <string>
#include <sstream>
#include <pthread.h>
#include <sys/timeb.h>
#include <stdint.h>
#include <unistd.h>
#include <cstring>
#include <errno.h>
#include <boost/foreach.hpp>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <algorithm>
#include <netinet/tcp.h>

#include "trace.hh"
#include "qq.hh"

#define CHECK(err, msg) \
	if ((err) < 0) \
		throw QQ(string(msg) + ": " + strerror(errno));

using namespace std;

struct ConnFunArg
{
	Connection *connection;
	sockaddr_in server;
	uint16_t port;
	unsigned quantum;
	
	ConnFunArg(Connection *connection, sockaddr_in server, uint16_t port, unsigned quantum)
		: connection(connection), server(server), port(port), quantum(quantum) {}
};

uint64_t timeMillis()
{
	timeb now;
	
	ftime(&now);
	return (uint64_t)now.time + now.millitm;
}

sockaddr_in hostnameToIP(const char *hostname)
{
	int err;
	addrinfo hints;
	addrinfo *servinfo;
	sockaddr_in ret;
	
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	
again:
	if ((err = getaddrinfo(hostname, NULL, &hints, &servinfo)) != 0)
	{
		if (err == EAI_AGAIN)
			goto again;
		
		throw QQ(string("getaddrinfo: ") + gai_strerror(err));
	}
	
	ret = *(reinterpret_cast<sockaddr_in *>(servinfo->ai_addr));
	
	freeaddrinfo(servinfo);
	
	return ret;
}

void *connFun(void *rawArg)
{
	static const size_t PAYLOAD_SIZE = 1460;
	static char payload[PAYLOAD_SIZE]; //garbage
	
	ConnFunArg *arg = reinterpret_cast<ConnFunArg *>(rawArg);
	int sock;
	int iterations;
	bool first = true;
	uint64_t start = timeMillis();
	int bytesSent = 0;
	
	if (arg->connection->duration == 0)
	{
		iterations = 1;
	}
	else
	{
		iterations = arg->connection->bytes / arg->connection->duration;
		if (arg->connection->bytes % arg->connection->duration)
			iterations += 1;
	}
	
	for (int i = 0; i < iterations; i++)
	{
		uint64_t iterationDue = start + arg->quantum * (i + 1);
		int64_t sleepDuration;
		size_t bytesDue;
		
		if (arg->connection->duration == 0)
		{
			bytesDue = arg->connection->bytes;
		}
		else
		{
			bytesDue = arg->connection->bytes * (iterationDue - start) / arg->connection->duration;
			if (bytesDue % PAYLOAD_SIZE)
				bytesDue = bytesDue / PAYLOAD_SIZE * (PAYLOAD_SIZE + 1);
			if (bytesDue > arg->connection->bytes)
				bytesDue = arg->connection->bytes;
		}
		
		if (first)
		{
			int one = 1;
			
			CHECK(sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP), "socket");
			CHECK(setsockopt(sock, SOL_TCP, TCP_NODELAY,&one, sizeof(one)), "setsockopt(TCP_NODELAY)");
			CHECK(connect(sock, reinterpret_cast<sockaddr *>(&arg->server), sizeof(arg->server)), "connect");
			
			first = false;
		}
		
		while (bytesDue - bytesSent > 0)
		{
			ssize_t bytes = send(sock, payload, min<size_t>(PAYLOAD_SIZE, bytesDue - bytesSent), 0);
			if (bytes < 0)
			{
				if (errno == EINTR)
					continue;
				
				throw QQ(string("send: ") + strerror(errno));
			}
				
			bytesSent += bytes;
		}
		
		sleepDuration = (int64_t)iterationDue - timeMillis();
		if (sleepDuration > 0)
			usleep(sleepDuration * 1000);
	}
	
	CHECK(close(sock), "close");
	
	delete arg->connection;
	delete arg;
	
	return NULL;
}

void mainLoop(TraceReader *reader, const char *server, uint16_t hbPort, unsigned quantum)
{
	list<pthread_t> threads;
	bool first = true;
	Connection *conn;
	sockaddr_in serverIP = hostnameToIP(server);
	uint16_t port = htons(hbPort);
	
	while ((conn = reader->nextConnection()) != NULL)
	{
		uint64_t now = timeMillis();
		int64_t delta;
		int64_t sleepTime;
		pthread_t thread;
		
		if (first)
		{
			delta = now - conn->startTime;
			first = false;
		}
		
		sleepTime = now - conn->startTime - delta;
		
		if (sleepTime > 0)
			CHECK(usleep(sleepTime * 1000), "usleep");
		
		CHECK(pthread_create(&thread, NULL, connFun, new ConnFunArg(conn, serverIP, port, quantum)), "pthread_create");
	}
	
	BOOST_FOREACH(pthread_t thread, threads)
	{
		CHECK(pthread_join(thread, NULL), "pthread_join");
	}
}

void usage()
{
	cerr << "usage: freeplay <trace> <server> <port> <time quantum> <trace read method>" << endl;
	cerr << "\t" << "where trace read method is jit or preload" << endl;
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
	if (argc != 5)
		usage();
	
	try
	{
		TraceReader *reader;
		int quantum;
		int port;
		
		istringstream(argv[3]) >> port;
		if (port < 0 || port > 65535)
			usage();
		
		istringstream(argv[4]) >> quantum;
		if (quantum < 0)
			usage();
		
		if (string(argv[5]) == "jit")
			reader = new JITTraceReader(argv[1]);
		else if (string(argv[5]) == "preload")
			reader = new PreloadingTraceReader(argv[1]);
		else
			usage();
		
		mainLoop(reader, argv[2], port, quantum);
		delete reader;
	}
	catch (exception &e)
	{
		cerr << e.what() << endl;
		exit(EXIT_FAILURE);
	}
	
	return 0;
}

