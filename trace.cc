#include <boost/tokenizer.hpp>
#include <sstream>

#include "trace.hh"

using namespace std;
using namespace boost;

static uint64_t extractValue(const string &line)
{
	size_t equals = line.find_first_of("=");
	size_t comma = line.find_first_of(",");
	uint64_t ret;
	
	if (equals == string::npos || comma == string::npos)
		throw QQ(string("bad line in trace: ") + line);
	
	istringstream(line.substr(equals + 1, comma - 1)) >> ret;
	
	return ret;
}

/* this is super messy */
Connection::Connection(const string &line)
{
	//('68.230.92.212', '133.132.104.59', 80, 53686, 6): byte_cnt=5784, start_t=1389675600, duration=0, flags[ACK|RST|SYN|FIN]=True|False|True|True
	tokenizer<> tok(line);
	tokenizer<>::iterator it = tok.begin();
	
	for (int i = 0; i < 5; i++)
		++it;
	bytes = extractValue(*it);
	
	++it;
	startTime = extractValue(*it);
	
	++it;
	duration = extractValue(*it);
}

TraceReader::~TraceReader() {}

JITTraceReader::~JITTraceReader() {}

Connection *JITTraceReader::nextConnection()
{
	string line;
	
	if (trace.eof())
		return NULL;
	
	getline(trace, line);
	
	return new Connection(line);
}

PreloadingTraceReader::PreloadingTraceReader(const char *fileName)
{
	ifstream trace(fileName, std::ios_base::in);
	
	while (!trace.eof())
	{
		string line;
		
		getline(trace, line);
		connections.push_back(new Connection(line));
	}
}

PreloadingTraceReader::~PreloadingTraceReader() {}

Connection *PreloadingTraceReader::nextConnection()
{
	Connection *ret;
	
	if (connections.empty())
		return NULL;
	
	ret = connections.front();
	connections.pop_front();
	
	return ret;
}
