TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    qq.cc \
    trace.cc \
    main.cc

HEADERS += \
    qq.hh \
    trace.hh

LIBS += -lpthread
