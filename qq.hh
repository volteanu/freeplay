#ifndef QQ_HH
#define QQ_HH

#include <exception>
#include <string>

class QQ: public std::exception
{
	const char *qq;

public:
	QQ(const char *qq)
		: qq(qq) {}
	
	QQ(const std::string &qq)
		: qq(qq.c_str()) {}
	
	virtual const char *what() const throw();
};

#endif // QQ_HH
